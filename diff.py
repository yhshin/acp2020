import sympy as sp
from scipy.misc import derivative

'''
Differentiation

2020.04.06 Young-Han Shin
'''

# function to be differentiated
def f(x):
    return x**3+x**2
    
# forward difference algorithm
def forwardDifference(f,x,h):
    return (f(x+h)-f(x))/h

# central difference algorithm
def centralDifference(f,x,h):
    return (f(x+h/2)-f(x-h/2))/h

# extrapolated difference algorithm
def extrapolatedDifference(f,x,h):
    return (4*centralDifference(f,x,h/2)-centralDifference(f,x,h))/3

if __name__ == "__main__":
    x = 1
    h = 0.1
    t = sp.symbols('t')

    # analytic derivative 
    print("Analytic derivative     : ", sp.diff(f(t),t).subs(t,x))

    # three differences we studied  in class
    print("Forward difference      : ", forwardDifference(f, x, h))
    print("Central difference      : ", centralDifference(f, x, h))
    print("Extrapolated difference : ", extrapolatedDifference(f, x, h))

    # derivative with scipy
    print("scipy.misc.derivative   : ", derivative(f, x, dx=h/2, n=1, order=3))
