import diff
import numpy as np
import matplotlib.pyplot as plt

'''
Simple harmonic motion with the Euler method and the Verlet method

1) for Euler method
d/dt theta = omega
d/dt omega = -theta

2) for Verlet method
d^2/dt^2 theta = -theta

2020.04.20 Young-Han Shin
'''

# 
def euler(x,force,dt):
    return x + force * dt

def verlet(x0,x1,d2dt2,dt):
    return 2*x1 - x0 + d2dt2*dt*dt

n = 10000 # the number of iterations
dt = 0.01 # time step

# Euler method
t = 0                 # t = 0
theta_e = [np.pi/6,]  # theta(t=0)
omega_e = [0,]        # omega(t=0)
for i in range(1,n):
    dtheta = omega_e[i-1]  # d(theta)
    domega = -theta_e[i-1] # d(omega)
    theta_e.append(euler(theta_e[i-1], dtheta, dt))
    omega_e.append(euler(omega_e[i-1], domega, dt))
    t += dt

# Verlet method
t = 0                 # t = 0
theta_v = [np.pi/6,]  # theta(t=0)
omega_v = [0,]        # omega(t=0)
t += dt
theta_v.append(theta_v[0]+omega_v[0]*dt-0.5*theta_v[0]*dt*dt) # theta(t=dt)
omega_v.append((theta_v[1]-theta_v[0])/dt)                    # omega(t=dt)
t += dt
for i in range(2,n):
    d2theta = -theta_v[i-1]
    theta_v.append(verlet(theta_v[i-2], theta_v[i-1], d2theta, dt))
    omega_v.append((theta_v[i]-theta_v[i-1])/dt)
    t += dt

# Plot
plt.plot(theta_e,omega_e,'r-',label="Euler")
plt.plot(theta_v,omega_v,'b-',label="Verlet")
plt.legend(loc='upper left')
plt.axis('square')
plt.xlabel("theta")
plt.ylabel("omega")
plt.show()

fig = plt.figure()

ax1 = fig.add_subplot(2,1,1)
plt.plot(theta_e,'r-',label="theta_e")
plt.plot(theta_v,'b-',label="theta_v")
plt.legend(loc='upper left')
plt.ylabel("theta")

ax2 = fig.add_subplot(2,1,2)
plt.plot(omega_e,'r-',label="omega_e")
plt.plot(omega_v,'b-',label="omega_v")
plt.legend(loc='upper left')
plt.xlabel("time")
plt.ylabel("omega")
plt.show()
