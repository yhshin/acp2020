import diff
import numpy as np
import matplotlib.pyplot as plt

'''
Differentiation

2020.04.08 Young-Han Shin
'''

# function to be differentiated
def f(x):
    return np.sin(x)

def secondDerivative(f,x,h):
    return (f(x+h)-2*f(x)+f(x-h))/(h*h)

x = np.pi/2+1.0
h = np.array([10**(-i) for i in range(5)])
result = secondDerivative(f,x,h)

print("h      : ", h)
print("result : ", result)

plt.plot(h,result,'r-o')
plt.show()
