import numpy as np

def str_int(x):
    return [int(x) for x in x.split()]

def str_float(x):
    return [float(x) for x in x.split()]

def str_str(x):
    return [x for x in x.split()]

def reading_POSCAR():

    fp = open('POSCAR','r')
    lines = fp.readlines()
    n_lines = {'l_unit':1, 'lv':[2,3,4], 'spec_names':5, 'spec_no':[6],'x':8}

    values = dict()

    # Lattice vectors
    l_unit = float(lines[n_lines['l_unit']])
    lv = []
    for n in n_lines['lv']:
        lv.append(str_float(lines[n]))
    lv = l_unit * np.array(lv)
    values['lv'] = lv

    # Species names
    spec_names = str_str(lines[n_lines['spec_names']])
    values['spec_names'] = spec_names

    # Number of species
    n_specs = len(spec_names)
    values['n_specs'] = n_specs

    # Number of atoms per species
    n_atoms_per_spec = str_int(lines[n_lines['spec_no'][0]])
    values['spec_no'] = n_atoms_per_spec

    # Number of atoms
    n_atoms = sum(n_atoms_per_spec)
    values['n_atoms'] = n_atoms

    # Positions
    x = list()
    i = n_lines['x']
    for i_spec in range(n_specs):
        tmp = list()
        for n in range(n_atoms_per_spec[i_spec]):
            tmp.append(str_float(lines[i]))
            i = i + 1
        x.append(tmp)
    values['x'] = np.array(x)

    # Velocities
    v = list()
    i = n_lines['x'] + n_atoms + 1
    for i_spec in range(n_specs):
        tmp = list()
        for n in range(n_atoms_per_spec[i_spec]):
            tmp.append(str_float(lines[i]))
            i = i + 1
        v.append(tmp)
    values['v'] = np.array(v)

    return values

def verlet(): # HW4 (1)
    pass

def check_equilibrium(): # HW4 (2)
    pass

def rescale_velocity(): # HW4 (4)
    pass

def kinetic_energy(): # HW4 (5)
    ke = 0
    return ke

def potential_energy(): # HW4 (6)
    pe = 0
    return pe

def calc_temperature(): # HW4 (7)
    temperature = 0
    return temperature

# Reading POSCAR
values = reading_POSCAR()

# Initial values of position and velocity
x = values['x']
v = values['v']
lv = values['lv']

#--------------------#
# Control parameters #
#--------------------#
n_rescale = 10000        # the number of iterations for rescaling
n_equilibrium = 10000    # the number of iterations for equilibrium
n_average = 10000        # the number of iterations for average
dt = 0.01                # time increase for integration
target_temperature = 300 # target temperature (K)
cutoff = 3.0             # for both Ar and Kr
mass = [39.948, 83.798]  # atomic masses for Ar and Kr in order (g/mol)
sigma = [3.41, 3.83]     # sigma for Ar-Ar and Kr-Kr in order
                         # sigma for A-B = 1/2(sigma_A + sigma_B)
epsilon = [119.8, 164.0] # epsilon for Ar-Ar and Kr-Kr in order
                         # epsilon for A-B = sqrt(sigma_A * sigma_B)

#--------------------#
# Velocity rescaling #
#--------------------#
for i in range(n_rescale):
    rescale_velocity()
    verlet()

#--------------------#
# Equilibrium        #
#--------------------#
for i in range(n_equilibrium):
    verlet()
    check_equilibrium()

#--------------------#
# Average            #
#--------------------#
temperature = 0
for i in range(n_average):
    verlet()
    calc_temperature()
