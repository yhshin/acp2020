import sympy as sp
import numpy as np
from scipy.integrate import simps
from scipy.integrate import trapz
from scipy.integrate import fixed_quad

'''
Integration

2020.04.15 Young-Han Shin
'''

# function to be integrated
def f(x):
    return x**4+x**3+x**2
    
# Trapzoid rule
def trapzoid(f,a,b,n):
    h = (b-a)/(n-1)
    x = np.linspace(a,b,n)
    y = f(x)
    return h*(sum(y)-(y[0]+y[n-1])*0.5)

# Simpson's rule
def simpson(f,a,b,n):
    h = (b-a)/(n-1)
    w = np.ones(n)
    for i in range(1,n-1):
        if i%2==0:
            w[i] = 2
        else:
            w[i] = 4
    x = np.linspace(a,b,n)
    y = f(x)
    return h/3*sum(w*y)

# Gaussian quadrature (2nd)
def gq2(f,a,b):
    w0 = w1 = 1
    x0 = (b-a)/2*(-1/np.sqrt(3))+(a+b)/2
    x1 = (b-a)/2*(+1/np.sqrt(3))+(a+b)/2
    return (b-a)/2*(w0*f(x0)+w1*f(x1))

if __name__ == "__main__":
    a = 0
    b = 1
    t = sp.symbols('t')

    # analytic integration
    print("Analytic integration        : ", sp.integrate(f(t),(t,a,b)).evalf())

    n = 10
    # Trapzoid with algorithm
    print("Trapzoid rule               : ", trapzoid(f,a,b,n))
    # trapzoid with scipy
    x = np.linspace(a,b,n)
    y = f(x)
    print("Trapzoid rule (scipy)       : ", trapz(y,x))

    n = 5
    # Simpson's with algorithm
    print("Simpson's rule              : ", simpson(f,a,b,2*n+1))
    # Simpson's with scipy
    x = np.linspace(a,b,2*n+1)
    y = f(x)
    print("Simpson's rule (scipy)      : ", simps(y,x))

    # Gaussian quadrature with algorithm
    print("Gaussian quadrature         : ", gq2(f,a,b))
    # Gaussian quadrature with scipy
    print("Gaussian quadrature (scipy) : ", fixed_quad(f,a,b,n=2))
    print("Gaussian quadrature (scipy) : ", fixed_quad(f,a,b,n=5))
